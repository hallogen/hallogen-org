var gulp = require("gulp");
var raml = require("gulp-raml");
var raml2html = require("raml2html");
var eventStream = require("event-stream");
var livereload = require("gulp-livereload");
var injectReload = require("gulp-inject-reload");
var open = require("gulp-open");
var runSequence = require("run-sequence");
var plumber = require("gulp-plumber");
var gutil = require("gulp-util");
var File = gutil.File;
var noop = gutil.noop;

var ramlMain = "./hallogen.raml";
var ramlFiles = [ "*.raml", "*.yaml" ];
var watching = false;

function ramlValidate() {
    return gulp.src(ramlMain)
        // Fixes the watch task stopping when an error occurs.
        .pipe(plumber({
            errorHandler: function(error) {
                this.emit("end");
            }
        }))
        .pipe(raml())
        .pipe(raml.reporter("default"))
        .pipe(raml.reporter("fail"))
}

function ramlRenderHtml() {
    return eventStream.map(function(file, callback) {
            var config = raml2html.getDefaultConfig();
            raml2html.render(file.contents, config,
                function(htmlData) {
                    var htmlFile = new File({
                            base: file.base,
                            cwd: file.cwd,
                            path: gutil.replaceExtension(file.path, ".html"),
                            contents: new Buffer(htmlData)
                        });
                    callback(null, htmlFile);
                },
                function(error) {
                    console.error(error);
                    callback(error);
                });
        });
}

gulp.task("raml", function() {
    return ramlValidate()
        .pipe(ramlRenderHtml())
        .pipe(watching ? injectReload() : noop())
        .pipe(gulp.dest("./doc"))
        .pipe(watching ? livereload() : noop())
});

gulp.task("watch", function() {
    watching = true;
    livereload.listen();
    runSequence("raml", function() {
        gulp.watch(ramlFiles, [ "raml" ]);
    });
});

gulp.task("default", [ "watch" ]);
